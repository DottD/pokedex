import Foundation
import PGoApi

class PokemonListViewController : UIViewController, PGoApiDelegate {
    // MARK: Properties
    @IBOutlet weak var MenuButton: UIButton!
    var Auth : PGoAuth?
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check existence of superview controller SWRevealViewController
        if self.revealViewController() != nil {
            // Link MenuButton with the menu opening
            self.MenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchDown)
            // Add a left-to-right swipe recognizer to open the menu
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Add a tap gesture recognizer to close the menu
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: PGoApiDelegate
    func didReceiveApiResponse(intent: PGoApiIntent, response: PGoApiResponse) {
//        switch intent {
//        case PGoApiIntent.Login:
//            print("Login Intent...")
//            // Create request
//            let request = PGoApiRequest()
//            //            request.setLocation(37, longitude: -122, altitude: 0)
//            //            request.getMapObjects()
//            request.getInventory()
//            // Update API endpoint in the correct auth var and make request
//            switch AccessType.selectedSegmentIndex {
//            case 0:
//                AuthGPS.endpoint = "https://\((response.response as! Pogoprotos.Networking.Envelopes.ResponseEnvelope).apiUrl)/rpc"
//                print("New endpoint: \(AuthGPS.endpoint)")
//                request.makeRequest(.GetInventory, auth: AuthGPS, delegate: self)
//            case 1:
//                AuthPTC.endpoint = "https://\((response.response as! Pogoprotos.Networking.Envelopes.ResponseEnvelope).apiUrl)/rpc"
//                print("New endpoint: \(AuthPTC.endpoint)")
//                request.makeRequest(.GetInventory, auth: AuthPTC, delegate: self)
//            default:
//                print("Access Type not recognized")
//            }
//        case PGoApiIntent.GetMapObjects:
//            print("Analysing \(response.subresponses.count) subresponses...")
//            for r in response.subresponses {
//                let r = r as! Pogoprotos.Networking.Responses.GetMapObjectsResponse
//                let cells = r.mapCells
//                print("Scanning \(cells.count) cells...")
//                for cell in cells {
//                    print(cell.nearbyPokemons)
//                    print(cell.wildPokemons)
//                    print(cell.catchablePokemons)
//                    print("-----------")
//                }
//            }
//        case PGoApiIntent.GetInventory:
//            print("Analyzing Inventory...")
//            for r in response.subresponses {
//                let r = r as! Pogoprotos.Networking.Responses.GetInventoryResponse
//                if r.hasSuccess && r.hasInventoryDelta {
//                    print("Found \(r.inventoryDelta.inventoryItems.count) Items")
//                    Inventory += r.inventoryDelta.inventoryItems
//                } else {
//                    print("Failed to get inventory!")
//                }
//            }
//        default:
//            print("Erroneous PGoApiIntent")
//        }
//        
//        // Stop animating activity indicator
//        self.LoginActivity.stopAnimating()
    }
    
    func didReceiveApiError(intent: PGoApiIntent, statusCode: Int?) {
//        print("API ERROR")
//        
//        // Stop animating activity indicator
//        self.LoginActivity.stopAnimating()
    }
    
}