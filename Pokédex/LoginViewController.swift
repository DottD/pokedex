//
//  LoginViewController.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 29/07/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import UIKit
import PGoApi

class LoginViewController: UIViewController, UITextFieldDelegate, PGoAuthDelegate, PGoApiDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var Username: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var AccessType: UISegmentedControl!
    @IBOutlet weak var LoginActivity: UIActivityIndicatorView!
    
    let highlightColor = UIColor(colorLiteralRed: 204, green: 229, blue: 255, alpha: 0)
    
    var Auth : PGoAuth?
    
    // MARK: Actions
    @IBAction func LoginPressed(sender: UIButton) {
        // Connectivity check
        
        
        if self.Username.hasText() && self.Password.hasText() {
            // Start activity indicator animation
            self.LoginActivity.startAnimating()
            
            // Attempt to log on niantic servers
            //        let name = self.Username.text!
            //        let pw = self.Password.text!
            let name = "Jecko90"
            let pw = "e5o-qb2-DC2-xoa"
            
            // Login attempt
            switch self.AccessType.selectedSegmentIndex {
            case 0:
                let AuthGPS = GPSOAuth()
                AuthGPS.delegate = self
                AuthGPS.login(withUsername: name, withPassword: pw)
                self.Auth = AuthGPS
            case 1:
                let AuthPTC = PtcOAuth()
                AuthPTC.delegate = self
                AuthPTC.login(withUsername: name, withPassword: pw)
                self.Auth = AuthPTC
            default:
                print("Access Type not recognized")
            }
        }
        else {
            self.HighlightEmptyFields()
        }
    }
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set this ViewController the delegate of the UITextField Username and Password
        self.Username.delegate = self
        self.Password.delegate = self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Authentication and login handling
    
    func didReceiveAuth() {
        print("Authentication Succeeded!")
        
        // Log in
        let LoginRequest = PGoApiRequest()
        LoginRequest.simulateAppStart()
        LoginRequest.makeRequest(.Login, auth: self.Auth!, delegate: self)
    }
    
    func didNotReceiveAuth() {
        print("Authentication Failed!")
        
        // If authentication is unsuccessful then alert the user
        HighlightEmptyFields()
    }
    
    func didReceiveApiResponse(intent: PGoApiIntent, response: PGoApiResponse) {
        if intent == .Login {
            Auth?.endpoint = "https://\((response.response as! Pogoprotos.Networking.Envelopes.ResponseEnvelope).apiUrl)/rpc"
            
            // Stop animating activity indicator
            self.LoginActivity.stopAnimating()
            
            // Pass to the following view
            performSegueWithIdentifier("LoggedInSegue", sender: self)
        } else {
            print("Only login intent in this class")
        }
    }
    
    func didReceiveApiError(intent: PGoApiIntent, statusCode: Int?) {
        print("Api error!")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let Dest = segue.destinationViewController as? SWRevealViewController
        let FrontVC = Dest?.frontViewController as? PokemonListViewController
        FrontVC?.Auth = self.Auth
    }
    
    
    // MARK: Text fields highlighting when empty
    
    func HighlightEmptyFields() {
        if !self.Username.hasText() {
            self.Username.backgroundColor = highlightColor
        }
        if !self.Password.hasText() {
            self.Password.backgroundColor = highlightColor
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        // Reset white background if in current TextField begins editing
        if textField.backgroundColor == highlightColor {
            textField.backgroundColor = UIColor.whiteColor()
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Handle connection among text fields
        textField.resignFirstResponder()
        if textField === self.Username {
            // When the user finished with the Username, pass to the Password
            self.Password.becomeFirstResponder()
        } else if textField === self.Password {
            // When the user finished also with the Password, hide the keyboard
            self.Password.resignFirstResponder()
        }
        return true
    }
    
}

