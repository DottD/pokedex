import Foundation
import PGoApi

class InventoryViewController : UIViewController, PGoApiDelegate {
    
    // MARK: Properties
    @IBOutlet weak var MenuButton: UIButton!
    @IBOutlet weak var InventoryTable: UITableView!
    
    var Auth : PGoAuth?
    var Inventory = [Pogoprotos.Inventory.InventoryItem()] {
        didSet {
            for Item in Inventory {
                if Item.hasInventoryItemData {
                    if Item.inventoryItemData.hasCandy {
                        InventoryTable.beginUpdates()
                        let Path = NSIndexPath(forItem: 1, inSection: 1)
                        InventoryTable.insertRowsAtIndexPaths([Path], withRowAnimation: UITableViewRowAnimation.Bottom)
                        InventoryTable.cellForRowAtIndexPath(Path)
                        InventoryTable.endUpdates()
                    } else if Item.inventoryItemData.hasPlayerCamera {
                        
                    }
                    
                } else {
                    print("Object without item data")
                }
                
                
                
                
                do {
                    try print(Item.getDescription(""))
                } catch {
                    print("Error")
                }
            }
        }
    }
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check existence of superview controller SWRevealViewController
        if self.revealViewController() != nil {
            // Link MenuButton with the menu opening
            self.MenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchDown)
            // Add a left-to-right swipe recognizer to open the menu
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Add a tap gesture recognizer to close the menu
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        // Ask the server for inventory informations
        
        
        // Display info in the table view
        InventoryTable.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: PGoApiDelegate
    
    func didReceiveApiResponse(intent: PGoApiIntent, response: PGoApiResponse) {
        print("Analyzing Inventory...")
        Inventory.removeAll()
        for SubResp in response.subresponses {
            let InvResp = SubResp as! Pogoprotos.Networking.Responses.GetInventoryResponse
            if InvResp.hasSuccess && InvResp.hasInventoryDelta {
                Inventory += InvResp.inventoryDelta.inventoryItems
            } else {
                print("Failed to get inventory!")
            }
        }
    }
    
    func didReceiveApiError(intent: PGoApiIntent, statusCode: Int?) {
        print("Api Error!")
    }
    
}