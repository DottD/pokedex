//
//  CommonProcedures.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 13/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import UIKit
import PGoApi
import CoreLocation

class ServerDelegate: NSObject, NSCoding, PGoApiDelegate, PGoAuthDelegate, CLLocationManagerDelegate {
    // MARK: Type alias
    // Collection types
    typealias InventoryCollection = [InventoryData]
    typealias PokemonCollection = [Int32: [PokemonData]]
    typealias PokedexCollection = [Int32: PokedexData]
    typealias CandiesCollection = [Int32: CandiesData]
    // Credentials structure
    typealias CredentialsType = (access: PGoAuthType, username: String, password: String)
    
    // MARK: Properties
    var Caller = UIViewController()
    var Inventory : InventoryCollection = []
    var Pokemon : PokemonCollection = [:]
    var Pokedex : PokedexCollection = [:]
    var Candies : CandiesCollection = [:]
    var UpdatedDate = NSDate()
    
    private var Auth : PGoAuth?
    private var Request : PGoApiRequest?
    private var locationDelegate = GPSDelegate()
    
    // MARK: Archiving Paths
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("CurrentState")
    
    enum SavedKey: String {
        case InventoryCount
        case InventoryItem
        
        case PokemonCount
        case PokemonType
        case PokemonFamily
        
        case PokedexCount
        case PokedexType
        case PokedexFamily
        
        case CandiesCount
        case CandiesType
        case CandiesFamily
        
        case MainAccess
        case MainUsername
        case MainPassword
        
        case PokemapAccess
        case PokemapUsername
        case PokemapPassword
        
        case UpdatedDate
    }
    
    enum StoredData {
        case Inventory
        case Pokemon
        case Pokedex
        case All
    }
    
    let MaxNumPokemon = 151
    
    // MARK: Login prop and methods
    var MainCredentials : CredentialsType = (.Google, "", "")
    var PokemapCredentials : CredentialsType = (.Google, "", "")
    enum LoginPurpose {
        case Main
        case Pokemap
    }
    enum ServerRequests {
        case InventoryAndPokemon
        case Pokemap
        case RenamePokemon
    }
    
    // MARK: Post authentication function handles
    var PostAuthSuccess: ()->Void = {}
    var PostAuthFail: ()->Void = {}
    var PostLoginSuccess: ()->Void = {}
    var PostLoginFail: ()->Void = {}
    var PostInventorySuccess: ()->Void = {}
    var PostNickSuccess: ()->Void = {}
    
    // MARK: User registration methods
    
    func registerUser(withPurpose purpose: LoginPurpose, withAccessType access: PGoAuthType, withUsername name: String, withPassword pw: String) {
        switch purpose {
        case .Main:
            MainCredentials = (access, name, pw)
        case .Pokemap:
            PokemapCredentials = (access, name, pw)
        }
    }
    
    func isUserRegistered(withPurpose purpose: LoginPurpose) -> Bool {
        switch purpose {
        case .Main:
            if MainCredentials.username != "" && MainCredentials.password != "" {
                return true
            } else {
                return false
            }
        case .Pokemap:
            if PokemapCredentials.username != "" && PokemapCredentials.password != "" {
                return true
            } else {
                return false
            }
        }
    }
    
    // MARK: persistent data handling
    
    private func get(key key: SavedKey, forType: Int32? = nil, atIndex: Int? = nil) -> String {
        var FinalKey = key.rawValue
        if let type = forType {
            FinalKey = String(type) + "_" + FinalKey
        }
        if let index = atIndex {
            return String(index) + "_" + FinalKey
        }
        print(FinalKey)
        return FinalKey
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        // Encode inventory: encode first the number of its elements, then each of them separately
        aCoder.encodeInteger(Inventory.count, forKey: get(key: .InventoryCount))
        var InventoryGen = Inventory.generate()
        for InventoryIndex in 0..<Inventory.count {
            if let InventoryItem = InventoryGen.next() {
                aCoder.encodeObject(InventoryItem, forKey: get(key: .InventoryItem, forType: nil, atIndex: InventoryIndex))
            }
        }
        
        // Encode pokemon: encode first the number of its elements, then each of them separately
        aCoder.encodeInteger(Pokemon.count, forKey: get(key: .PokemonCount))
        var PokemonFamilyGen = Pokemon.generate()
        for PokemonFamilyIndex in 0..<Pokemon.count {
            if let (FamilyID, FamilyDataArray) = PokemonFamilyGen.next() {
                aCoder.encodeInt32(FamilyID, forKey: get(key: .PokemonFamily, forType: nil, atIndex: PokemonFamilyIndex))
                aCoder.encodeInteger(FamilyDataArray.count, forKey: get(key: .PokemonCount, forType: FamilyID, atIndex: nil))
                var PokemonGen = FamilyDataArray.generate()
                for PokemonIndex in 0..<FamilyDataArray.count {
                    aCoder.encodeObject(PokemonGen.next(), forKey: get(key: .PokemonType, forType: FamilyID, atIndex: PokemonIndex))
                }
            }
        }
        
        // Encode pokedex: encode first the number of its elements, then each of them separately
        aCoder.encodeInteger(Pokedex.count, forKey: get(key: .PokedexCount))
        var PokedexGen = Pokedex.generate()
        for PokedexIndex in 0..<Pokedex.count {
            if let (FamilyID, PokedexData) = PokedexGen.next() {
                aCoder.encodeInt32(FamilyID, forKey: get(key: .PokedexFamily, forType: nil, atIndex: PokedexIndex))
                aCoder.encodeObject(PokedexData, forKey: get(key: .PokedexType, forType: nil, atIndex: PokedexIndex))
            }
        }
        
        // Encode candies: encode first the number of its elements, then each of them separately
        aCoder.encodeInteger(Candies.count, forKey: get(key: .CandiesCount))
        var CandiesGen = Candies.generate()
        for CandiesIndex in 0..<Candies.count {
            if let (FamilyID, CandiesData) = CandiesGen.next() {
                aCoder.encodeInt32(FamilyID, forKey: get(key: .CandiesFamily, forType: nil, atIndex: CandiesIndex))
                aCoder.encodeObject(CandiesData, forKey: get(key: .CandiesType, forType: nil, atIndex: CandiesIndex))
            }
        }
        
        // Encode the usernames and passwords
        aCoder.encodeObject(MainCredentials.access.description, forKey: SavedKey.MainAccess.rawValue)
        aCoder.encodeObject(MainCredentials.username, forKey: SavedKey.MainUsername.rawValue)
        aCoder.encodeObject(MainCredentials.password, forKey: SavedKey.MainPassword.rawValue)
        
        aCoder.encodeObject(PokemapCredentials.access.description, forKey: SavedKey.PokemapAccess.rawValue)
        aCoder.encodeObject(PokemapCredentials.username, forKey: SavedKey.PokemapUsername.rawValue)
        aCoder.encodeObject(PokemapCredentials.password, forKey: SavedKey.PokemapPassword.rawValue)
        
        aCoder.encodeObject(UpdatedDate, forKey: SavedKey.UpdatedDate.rawValue)
    }
    
    required convenience init? (coder aDecoder: NSCoder) {
        self.init()
        
        // Recreate Inventory
        self.Inventory.removeAll()
        let InventoryCount = aDecoder.decodeIntegerForKey(get(key: .InventoryCount))
        for InventoryIndex in 0..<InventoryCount {
            self.Inventory.append(aDecoder.decodeObjectForKey(get(key: .InventoryItem, forType: nil, atIndex: InventoryIndex)) as! InventoryData)
        }
        
        // Recreate Pokemon
        self.Pokemon.removeAll()
        let PokemonCount = aDecoder.decodeIntegerForKey(get(key: .PokemonCount))
        for PokemonFamilyIndex in 0..<PokemonCount {
            let FamilyID = aDecoder.decodeInt32ForKey(get(key: .PokemonFamily, forType: nil, atIndex: PokemonFamilyIndex))
            let FamilyDataArrayCount = aDecoder.decodeIntegerForKey(get(key: .PokemonCount, forType: FamilyID, atIndex: nil))
            var FamilyDataArray = [PokemonData]()
            for PokemonIndex in 0..<FamilyDataArrayCount {
                let Data = aDecoder.decodeObjectForKey(get(key: .PokemonType, forType: FamilyID, atIndex: PokemonIndex)) as! PokemonData
                FamilyDataArray.append(Data)
            }
            self.Pokemon[FamilyID] = FamilyDataArray
        }
        
        // Recreate Pokedex
        self.Pokedex.removeAll()
        let PokedexCount = aDecoder.decodeIntegerForKey(get(key: .PokedexCount))
        for PokedexIndex in 0..<PokedexCount {
            let FamilyID = aDecoder.decodeInt32ForKey(get(key: .PokedexFamily, forType: nil, atIndex: PokedexIndex))
            let Data = aDecoder.decodeObjectForKey(get(key: .PokedexType, forType: nil, atIndex: PokedexIndex)) as! PokedexData
            self.Pokedex[FamilyID] = Data
        }
        
        // Recreate Candies
        self.Candies.removeAll()
        let CandiesCount = aDecoder.decodeIntegerForKey(get(key: .CandiesCount))
        for CandiesIndex in 0..<CandiesCount {
            let FamilyID = aDecoder.decodeInt32ForKey(get(key: .CandiesFamily, forType: nil, atIndex: CandiesIndex))
            let Data = aDecoder.decodeObjectForKey(get(key: .CandiesType, forType: nil, atIndex: CandiesIndex)) as! CandiesData
            self.Candies[FamilyID] = Data
        }
        
        // Recreate the main credentials info
        var name = aDecoder.decodeObjectForKey(SavedKey.MainUsername.rawValue) as! String
        var pw = aDecoder.decodeObjectForKey(SavedKey.MainPassword.rawValue) as! String
        var desc = aDecoder.decodeObjectForKey(SavedKey.MainAccess.rawValue) as! String
        switch desc {
        case PGoAuthType.Google.description:
            self.MainCredentials = (.Google, name, pw)
        case PGoAuthType.Ptc.description:
            self.MainCredentials = (.Ptc, name, pw)
        default:
            fatalError("Main credentials cannot be retreived")
        }
        
        // Recreate the pokemap credentials info
        name = aDecoder.decodeObjectForKey(SavedKey.PokemapUsername.rawValue) as! String
        pw = aDecoder.decodeObjectForKey(SavedKey.PokemapPassword.rawValue) as! String
        desc = aDecoder.decodeObjectForKey(SavedKey.PokemapAccess.rawValue) as! String
        switch desc {
        case PGoAuthType.Google.description:
            self.PokemapCredentials = (.Google, name, pw)
        case PGoAuthType.Ptc.description:
            self.PokemapCredentials = (.Ptc, name, pw)
        default:
            fatalError("Pokemap credentials cannot be retreived")
        }
        
        self.UpdatedDate = aDecoder.decodeObjectForKey(SavedKey.UpdatedDate.rawValue) as! NSDate
    }
    
    func saveState() -> Bool {
        return NSKeyedArchiver.archiveRootObject(self, toFile: ServerDelegate.ArchiveURL.path!)
    }
    
    func loadState() -> Bool {
        let LoadedState = NSKeyedUnarchiver.unarchiveObjectWithFile(ServerDelegate.ArchiveURL.path!) as? ServerDelegate
        if LoadedState != nil {
            // Assign values to each property
            self.Inventory = (LoadedState?.Inventory)!
            self.Pokemon = (LoadedState?.Pokemon)!
            self.Pokedex = (LoadedState?.Pokedex)!
            self.Candies = (LoadedState?.Candies)!
            self.UpdatedDate = (LoadedState?.UpdatedDate)!
            self.MainCredentials = (LoadedState?.MainCredentials)!
            self.PokemapCredentials = (LoadedState?.PokemapCredentials)!
            return true
        } else {
            return false
        }
    }
    
    func deleteState() -> Bool {
        let FileManager = NSFileManager.defaultManager()
        do {
            try FileManager.removeItemAtURL(ServerDelegate.ArchiveURL)
            return true
        } catch {
            return false
        }
    }
    
    // MARK: Server requests and responses
    
    func askServer(request request: ServerRequests, withPokemonId pokeid: UInt64 = UInt64.max, toNewName name: String = "") {
        switch request {
        case .InventoryAndPokemon:
            // Ask the GPS for the user position and then ask the server for inventory and pokemons
            self.locationDelegate.PostLocationUpdateSuccess = {(location) in
                // Create a new request
                self.Request = PGoApiRequest(auth: self.Auth!)
                // Make the inventory request
                self.Request!.getInventory()
                // Set the in-game location of the player based on real location to avoid ban (required bu pgoapi)
                self.Request!.setLocation(location.coordinate.latitude, longitude: location.coordinate.longitude, altitude: 1.0)
                // Send the specified request
                self.Request!.makeRequest(.GetInventory, delegate: self)
            }
            self.locationDelegate.RequestCurrentLocation()
        case .Pokemap:
            print("ciao")
        case .RenamePokemon:
            if pokeid != UInt64.max { // Default arguments are not allowed
                self.Request!.nicknamePokemon(pokeid, nickname: name)
                self.Request!.makeRequest(.NicknamePokemon, delegate: self)
            }
        }
    }
    
    func didReceiveApiResponse(intent: PGoApiIntent, response: PGoApiResponse) {
        // Add inventory data to the inventory array for further uses
        switch intent {
        case .GetInventory:
            // Prepare the containers for new data
            self.Inventory.removeAll()
            self.Pokemon.removeAll()
            self.Pokedex.removeAll()
            self.Candies.removeAll()
            // Fill the inventory with the info provided by the servers
            for SubResp in response.subresponses {
                if let InvResp = SubResp as? Pogoprotos.Networking.Responses.GetInventoryResponse {
                    if InvResp.hasSuccess && InvResp.hasInventoryDelta {
                        for Item in InvResp.inventoryDelta.inventoryItems {
                            if Item.hasInventoryItemData {
                                if let ItemData = Item.inventoryItemData {
                                    if ItemData.hasItem {
                                        if let ItemEntry = ItemData.item {
                                            Inventory.append(InventoryData(fromItemData: ItemEntry))
                                        }
                                    } else if ItemData.hasPokemonData {
                                        if let pokemonData = ItemData.pokemonData {
                                            // Does not add eggs
                                            if !pokemonData.isEgg {
                                                if pokemonData.hasPokemonId {
                                                    let FamilyID = pokemonData.pokemonId.rawValue
                                                    if self.Pokemon[FamilyID] != nil {
                                                        // Already exist this family array
                                                        self.Pokemon[FamilyID]!.append(PokemonData(fromPokemonData: pokemonData))
                                                    } else {
                                                        // Create a new family array
                                                        self.Pokemon[FamilyID] = [PokemonData(fromPokemonData: pokemonData)]
                                                    }
                                                }
                                            }
                                        }
                                    } else if ItemData.hasPokedexEntry {
                                        if let pokedexEntry = ItemData.pokedexEntry {
                                            if pokedexEntry.hasPokemonId {
                                                let FamilyID = pokedexEntry.pokemonId.rawValue
                                                self.Pokedex[FamilyID] = PokedexData(fromPokedexEntry: pokedexEntry)
                                            }
                                        }
                                    } else if ItemData.hasCandy {
                                        if let Candy = ItemData.candy {
                                            if Candy.hasFamilyId {
                                                let FamilyID = Candy.familyId.rawValue
                                                self.Candies[FamilyID] = CandiesData(fromCandy: Candy)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        print("Failed to get inventory!")
                    }
                }
            }
            // For each section reorder according to overall IV
            let LambdaSortSection = { (a: PokemonData, b: PokemonData) -> Bool in
                var aOverall: Int
                var bOverall: Int
                if let aAttack = a.getIndividualAttack() {
                    if let aDefense = a.getIndividualDefense() {
                        if let aStamina = a.getIndividualStamina() {
                            if let bAttack = b.getIndividualAttack() {
                                if let bDefense = b.getIndividualDefense() {
                                    if let bStamina = b.getIndividualStamina() {
                                        aOverall = aAttack + aDefense + aStamina
                                        bOverall = bAttack + bDefense + bStamina
                                        return aOverall < bOverall
                                    }
                                }
                            }
                        }
                    }
                }
                fatalError("Some property is unset")
            }
            for var (_, Section) in Pokemon {
                Section.sortInPlace(LambdaSortSection)
            }
            
            // Change time of last update
            UpdatedDate = NSDate()
            
            // Save state after successful inventory response
            saveState()
            
            // Execute post inventory user defined function
            PostInventorySuccess()
        case .Login:
            // Save state after successful login response
            saveState()
            // Execute post login user defined function
            PostLoginSuccess()
        case .NicknamePokemon:
            // Change Pokemon fields according to newly renamed pokemon
            // Execute post nikname user defined function
            PostNickSuccess()
        default:
            fatalError("PGoApiIntent not handled")
        }
    }
    
    func didReceiveApiError(intent: PGoApiIntent, statusCode: Int?) {
        AppDelegate.PopupMessage(withTitle: "Error", withMessage: "Unable to reach servers", fromCaller: Caller)
    }
    
    func didReceiveApiException(intent: PGoApiIntent, exception: PGoApiExceptions) {
        print("PGoApi Exception:")
        switch exception {
        case .AuthTokenExpired:
            print("Authentication Token Expired!")
        case .Banned:
            print("The user has been banned!")
        case .DelayRequired:
            print("Wait please...")
        case .InvalidRequest:
            print("Invalid request!")
        case .NoApiMethodsCalled:
            print("No ApiMethod has been called")
        case .NoAuth:
            print("No authentication provided")
        case .NotLoggedIn:
            print("The user did not log in")
        case .SessionInvalidated:
            print("Session was invalidated")
        case .Unknown:
            print("Unknown authentication exception")
        }
    }
    
    // MARK: Authentication
    
    func authenticate(withPurpose purpose: LoginPurpose) {
        // Establish the credentials to authenticate with
        var credentials : CredentialsType
        switch purpose {
        case .Main:
            credentials = self.MainCredentials
        case .Pokemap:
            credentials = self.PokemapCredentials
        }
        // Perform authentication on server
        switch credentials.access {
        case .Google:
            let GoogleAccess = GPSOAuth()
            GoogleAccess.delegate = self
            GoogleAccess.login(withUsername: credentials.username, withPassword: credentials.password)
            self.Auth = GoogleAccess
        case .Ptc:
            let ClubAccess = PtcOAuth()
            ClubAccess.delegate = self
            ClubAccess.login(withUsername: credentials.username, withPassword: credentials.password)
            self.Auth = ClubAccess
        }
    }
    
    func didReceiveAuth() {
        // Execute user defined function
        PostAuthSuccess()
        // Log in
        self.Request = PGoApiRequest(auth: self.Auth!)
        self.Request!.simulateAppStart()
        self.Request!.makeRequest(.Login, delegate: self)
    }
    
    func didNotReceiveAuth() {
        // Execute user defined function
        PostAuthFail()
    }
    
    // MARK: Data handling
    
    func hasData(ofType type: StoredData) -> Bool {
        switch type {
        case .All:
            return !Inventory.isEmpty && !Pokemon.isEmpty && !Pokedex.isEmpty
        case .Inventory:
            return !Inventory.isEmpty
        case .Pokedex:
            return !Pokedex.isEmpty
        case .Pokemon:
            return !Pokemon.isEmpty
        }
    }
    
}
