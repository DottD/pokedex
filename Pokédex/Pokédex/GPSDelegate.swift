//
//  GPSDelegate.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 07/09/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import CoreLocation

class GPSDelegate: NSObject, CLLocationManagerDelegate {
    // MARK: Properties 
    var Caller = UIViewController()
    let LocationManager = CLLocationManager()
    // MARK: GPS messages
    let ErrorGPSTitle = "GPS Error"
    let ErrorGPSMessageFail = "Unable to retrieve user's position"
    let ErrorGPSMessageAuthorization = "Authorization denied: this app won't work!"
    // MARK: Private Properies
    var PostLocationUpdateSuccess: (CLLocation)->Void = {(_) in }
    var PostLocationUpdateFail: ()->Void = {}
    
    // MARK: Initialization
    override init() {
        super.init()
        // Set this class as the delegate for LocationManager
        LocationManager.delegate = self
    }
    
    // MARK: GPS handling
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // Execute post update location function
        PostLocationUpdateSuccess(locations.last!)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        // Execute post update location function
        PostLocationUpdateFail()
        
        // Show the error to the user
        print("\(error.localizedDescription)  \(error.localizedFailureReason)")
        AppDelegate.PopupMessage(withTitle: ErrorGPSTitle, withMessage: ErrorGPSMessageFail, fromCaller: self)
    }
    
    // MARK: convenience methods
    func RequestCurrentLocation() {
        let status = CLLocationManager.authorizationStatus()
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            LocationManager.requestLocation()
        } else if status == .Restricted || status == .Denied {
            AppDelegate.PopupMessage(withTitle: ErrorGPSTitle, withMessage: ErrorGPSMessageAuthorization, fromCaller: self.Caller)
        } else {
            LocationManager.requestWhenInUseAuthorization()
        }
    }
}