//
//  PokemonCollectionViewCell.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 18/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import UIKit
import PGoApi

class PokemonCollectionViewCell : UICollectionViewCell {
    
    @IBOutlet weak var IV: UILabel!
    @IBOutlet weak var Sprite: PieIVView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var CP: UILabel!
    
}