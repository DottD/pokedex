//
//  PieIVView.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 21/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import UIKit

class PieIVView: UIView {
    // MARK: Properties
    // IV related to their maxima
    var Attack : CGFloat = 0.0
    var Defense : CGFloat = 0.0
    var Stamina : CGFloat = 0.0
    
    // Set drawing props
    let StartAngle = CGFloat(-M_PI_2)
    let LineWidth = CGFloat(4)
    let LineSep = CGFloat(2)
    
    // MARK: Updating functions
    func updateIV(toAttack attack: Int, toDefense defense: Int, toStamina stamina: Int) {
        self.Attack = CGFloat(Float(attack))/AppDelegate.MaxIV
        self.Defense = CGFloat(Float(defense))/AppDelegate.MaxIV
        self.Stamina = CGFloat(Float(stamina))/AppDelegate.MaxIV
    }
    
    // MARK: Drawing function
    override func drawRect(rect: CGRect) {
        
        // Compute the radius of graphic context
        let BaseRadius = min(rect.height/2.0, rect.width/2.0)

        // Draw a circle for attack property
        let AttackCircleRadius = BaseRadius - LineSep
        let AttackCircleEndAngle = StartAngle + self.Attack * 2.0 * CGFloat(M_PI)
        let AttackCirclePath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: AttackCircleRadius, startAngle: StartAngle, endAngle: AttackCircleEndAngle, clockwise: true)
        AttackCirclePath.lineWidth = LineWidth
        UIColor.redColor().setStroke()
        AttackCirclePath.stroke()
        
        // Draw a circle for defense property
        let DefenseCircleRadius = AttackCircleRadius - (LineWidth + LineSep) // subtract the first line width
        let DefenseCircleEndAngle = StartAngle + self.Defense * 2.0 * CGFloat(M_PI)
        let DefenseCirclePath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: DefenseCircleRadius, startAngle: StartAngle, endAngle: DefenseCircleEndAngle, clockwise: true)
        DefenseCirclePath.lineWidth = LineWidth
        UIColor.blueColor().setStroke()
        DefenseCirclePath.stroke()
        
        // Draw a circle for stamina property
        let StaminaCircleRadius = DefenseCircleRadius - (LineWidth + LineSep) // subtract the first two lines width
        let StaminaCircleEndAngle = StartAngle + self.Stamina * 2.0 * CGFloat(M_PI)
        let StaminaCirclePath = UIBezierPath(arcCenter: CGPoint(x: rect.midX, y: rect.midY), radius: StaminaCircleRadius, startAngle: StartAngle, endAngle: StaminaCircleEndAngle, clockwise: true)
        StaminaCirclePath.lineWidth = LineWidth
        UIColor.greenColor().setStroke()
        StaminaCirclePath.stroke()
    }
}