//
//  SettingsViewController.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 18/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController : UITableViewController, UITextFieldDelegate {
    
    // MARK: Properties
    @IBOutlet weak var MenuButton: PokeButton!
    
    static var EvoMark = "Evo"
    static var SellMark = "Sell"
    static var EvoSellMark = "EvoSell"
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var startAppBanner: STABannerView?
    
    @IBOutlet weak var EvoTextField: UITextField!
    @IBOutlet weak var SellTextField: UITextField!
    @IBOutlet weak var EvoSellTextField: UITextField!
    
    // MARK: Default functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set self to be the delegate of the UITextField
        EvoTextField.delegate = self
        SellTextField.delegate = self
        EvoSellTextField.delegate = self
        
        // Check existence of superview controller SWRevealViewController
        if self.revealViewController() != nil {
            // Link MenuButton with the menu opening
            self.MenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchDown)
            // Add a left-to-right swipe recognizer to open the menu
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Add a tap gesture recognizer to close the menu
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        // Load login information from file
        appDelegate.serverDelegate.loadState()
        
        // Setup StartApp Banner
        if (startAppBanner == nil) {
            startAppBanner = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Bottom, withView: self.view, withDelegate: nil);
            self.view.addSubview(startAppBanner!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Link the table with actions
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                // Delete login info
                appDelegate.serverDelegate.deleteState()
                // Back to login screen
                performSegueWithIdentifier("LogOutSegue", sender: self)
            default:
                print("Case not implemented")
            }
        default:
            print("Case not implemented")
        }
    }
    
    // MARK: TextField Delegate
    
    func textFieldDidEndEditing(textField: UITextField) {
        switch textField.placeholder! {
        case "Evo":
            SettingsViewController.EvoMark = textField.text!
        case "Sell":
            SettingsViewController.SellMark = textField.text!
        case "EvoSell":
            SettingsViewController.EvoSellMark = textField.text!
        default:
            print("UITextField not recognised")
        }
    }
    
}