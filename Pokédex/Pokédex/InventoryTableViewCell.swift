//
//  InventoryTableViewCell.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 12/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation

class InventoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Sprite: UIImageView!
    @IBOutlet weak var Count: UILabel!
    
}