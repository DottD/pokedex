//
// Created by Filippo Santarelli on 05/09/16.
// Copyright (c) 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import UIKit

class PokeButton: UIButton {
    // MARK: Menu button animation config
    var MenuButtonAnimationImages : [UIImage] {
        get {
            let AnimationSteps = 12
            let StepAngle = 2.0*180.0/Double(AnimationSteps)
            var Images = [UIImage(named: "MenuButtonIcon")!]
            for n in 0...AnimationSteps {
                let RotationDegrees = CGFloat(Double(n)*StepAngle)
                let NewImage = Images.first!.imageRotatedByDegrees(RotationDegrees, flip: false, adaptiveFrame: false)
                Images.append(NewImage)
            }
            return Images
        }
    }
    
    // MARK: Behaviour
    
    // Initialization method
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Set properties
        self.imageView!.animationDuration = 0.5
        self.imageView!.animationRepeatCount = 1
        self.imageView!.animationImages = MenuButtonAnimationImages
    }
    
    // Start animation on touch
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.imageView?.startAnimating()
    }
}
