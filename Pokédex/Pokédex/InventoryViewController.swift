import Foundation
import UIKit
import PGoApi

class InventoryViewController : UITableViewController {
    
    // MARK: Properties
    @IBOutlet weak var MenuButton: PokeButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var startAppBanner: STABannerView?
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check existence of superview controller SWRevealViewController
        if self.revealViewController() != nil {
            // Link MenuButton with the menu opening
            self.MenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchDown)
            // Add a left-to-right swipe recognizer to open the menu
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Add a tap gesture recognizer to close the menu
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        // Check presence of data in serverDelegate
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if !appDelegate.serverDelegate.hasData(ofType: .Inventory) {
            appDelegate.serverDelegate.PostInventorySuccess = {
                self.tableView?.reloadData()
            }
            appDelegate.serverDelegate.askServer(request: .InventoryAndPokemon)
        }
        
        // Initialize the "pull to refresh" control
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.tintColor = AppDelegate.textColor
        self.refreshControl?.addTarget(self, action: #selector(InventoryViewController.RefreshByPull), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView?.alwaysBounceVertical = true;
        
        // Setup StartApp Banner
        if (startAppBanner == nil) {
            startAppBanner = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Bottom, withView: self.view, withDelegate: nil);
            self.view.addSubview(startAppBanner!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table handling
    
    // Returns the number of the items stored in Inventory
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appDelegate.serverDelegate.Inventory.count
    }
    
    // Fill the table with the informations in Inventory
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Take the model stored in a prototype cell
        let NewCell = tableView.dequeueReusableCellWithIdentifier("InventoryProtoCell") as! InventoryTableViewCell
        
        let Item = appDelegate.serverDelegate.Inventory[indexPath.row]
        if let Id = Item.getId() {
            if let Count = Item.getCount() {
                switch Id {
                case .ItemPokeBall:
                    NewCell.Name.text = "Pokéball"
                    NewCell.Sprite.image = UIImage(named: "Pokeball")
                case .ItemGreatBall:
                    NewCell.Name.text = "Greatball"
                    NewCell.Sprite.image = UIImage(named: "Greatball")
                case .ItemUltraBall:
                    NewCell.Name.text = "Ultraball"
                    NewCell.Sprite.image = UIImage(named: "Ultraball")
                case .ItemMasterBall:
                    NewCell.Name.text = "Masterball"
                    NewCell.Sprite.image = UIImage(named: "Masterball")
                case .ItemIncubatorBasicUnlimited:
                    NewCell.Name.text = "Incubator Unlimited"
                    NewCell.Sprite.image = UIImage(named: "IncubatorUnlimited")
                case .ItemIncubatorBasic:
                    NewCell.Name.text = "Incubator"
                    NewCell.Sprite.image = UIImage(named: "Incubator")
                case .ItemIncenseOrdinary:
                    NewCell.Name.text = "Incense"
                    NewCell.Sprite.image = UIImage(named: "Incense")
                case .ItemIncenseSpicy:
                    NewCell.Name.text = "Spicy Incense"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemIncenseCool:
                    NewCell.Name.text = "Cool Incense"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemIncenseFloral:
                    NewCell.Name.text = "Floral Incense"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemPotion:
                    NewCell.Name.text = "Potion"
                    NewCell.Sprite.image = UIImage(named: "Potion")
                case .ItemSuperPotion:
                    NewCell.Name.text = "Super Potion"
                    NewCell.Sprite.image = UIImage(named: "SuperPotion")
                case .ItemHyperPotion:
                    NewCell.Name.text = "Hyper Potion"
                    NewCell.Sprite.image = UIImage(named: "HyperPotion")
                case .ItemMaxPotion:
                    NewCell.Name.text = "Max Potion"
                    NewCell.Sprite.image = UIImage(named: "MaxPotion")
                case .ItemRazzBerry:
                    NewCell.Name.text = "Razz Berry"
                    NewCell.Sprite.image = UIImage(named: "RazzBerry")
                case .ItemBlukBerry:
                    NewCell.Name.text = "Bluk Berry"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemNanabBerry:
                    NewCell.Name.text = "Nanab Berry"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemPinapBerry:
                    NewCell.Name.text = "Pinap Berry"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemWeparBerry:
                    NewCell.Name.text = "Wepar Berry"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemRevive:
                    NewCell.Name.text = "Revive"
                    NewCell.Sprite.image = UIImage(named: "Revive")
                case .ItemMaxRevive:
                    NewCell.Name.text = "Max Revive"
                    NewCell.Sprite.image = UIImage(named: "MaxRevive")
                case .ItemLuckyEgg:
                    NewCell.Name.text = "Lucky Egg"
                    NewCell.Sprite.image = UIImage(named: "LuckyEgg")
                case .ItemSpecialCamera:
                    NewCell.Name.text = "Special Camera"
                    NewCell.Sprite.image = UIImage(named: "SpecialCamera")
                case .ItemTroyDisk:
                    NewCell.Name.text = "Troy Disk"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemXAttack:
                    NewCell.Name.text = "X Attack"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemXDefense:
                    NewCell.Name.text = "X Defense"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemXMiracle:
                    NewCell.Name.text = "X Miracle"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemItemStorageUpgrade:
                    NewCell.Name.text = "Item Storage Upgrade"
                    NewCell.Sprite.image = UIImage(named: "RazzBerry")
                case .ItemUnknown:
                    NewCell.Name.text = "Unknown"
                    NewCell.Sprite.image = UIImage(named: "Unknown")
                case .ItemPokemonStorageUpgrade:
                    NewCell.Name.text = "Pokémon Storage Upgrade"
                    NewCell.Sprite.image = UIImage(named: "PokemonUpgrade")
                }
                NewCell.Count.text = String(Count)
            }
        }
        
        return NewCell
    }
    
    // Make another request to the server to refresh the list
    func RefreshByPull() {
        // Ask to the server
        appDelegate.serverDelegate.PostInventorySuccess = {
            // Reload the table
            self.tableView.reloadData()
            
            // End refreshing
            self.refreshControl?.endRefreshing()
        }
        appDelegate.serverDelegate.askServer(request: .InventoryAndPokemon)
    }
    
}