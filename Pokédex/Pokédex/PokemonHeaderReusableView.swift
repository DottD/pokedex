//
//  PokemonHeaderReusableView.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 21/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation

class PokemonHeaderReusableView: UICollectionReusableView {
    
    @IBOutlet weak var PokemonName: UILabel!
    @IBOutlet weak var PokemonSprite: UIImageView!
}