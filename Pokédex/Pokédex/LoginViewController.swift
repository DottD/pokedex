//
//  LoginViewController.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 29/07/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import UIKit
import PGoApi

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var Username: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var AccessType: UISegmentedControl!
    @IBOutlet weak var LoginActivity: UIActivityIndicatorView!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var startAppBanner: STABannerView?
    
    // MARK: Actions
    @IBAction func LoginPressed(sender: UIButton) {
        if Username.hasText() && Password.hasText() {
            // Start activity indicator animation
            LoginActivity.startAnimating()
            
            // Attempt to log on niantic servers
//            let name = Username.text!
//            let pw = Password.text!
            let name = "Jecko90"
            let pw = "e5o-qb2-DC2-xoa"
            
            // Login attempt
            if AccessType.selectedSegmentIndex == 0 {
                appDelegate.serverDelegate.registerUser(withPurpose: .Main, withAccessType: .Google, withUsername: name, withPassword: pw)
            } else {
                appDelegate.serverDelegate.registerUser(withPurpose: .Main, withAccessType: .Ptc, withUsername: name, withPassword: pw)
            }
            
            // Perform authentication
            appDelegate.serverDelegate.PostAuthFail = PostAuthFail
            appDelegate.serverDelegate.PostLoginSuccess = PostLoginSuccess
            appDelegate.serverDelegate.authenticate(withPurpose: .Main)
        }
        else {
            HighlightEmptyFields()
        }
    }
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Stop animating the activity indicator
        LoginActivity.hidesWhenStopped = true
        
        // Set this ViewController as the delegate of the UITextField Username and Password
        Username.delegate = self
        Password.delegate = self
        
        // Setup StartApp Banner
        if (startAppBanner == nil) {
            startAppBanner = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Bottom, withView: self.view, withDelegate: nil);
            self.view.addSubview(startAppBanner!)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Text fields highlighting when empty
    
    func HighlightEmptyFields() {
        AppDelegate.normalColor = Username.backgroundColor!
        AppDelegate.normalColor = Password.backgroundColor!
        Username.backgroundColor = AppDelegate.highlightColor
        Password.backgroundColor = AppDelegate.highlightColor
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        // Reset white background if in current TextField begins editing
        if textField.backgroundColor == AppDelegate.highlightColor {
            textField.backgroundColor = AppDelegate.normalColor
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // Handle connection among text fields
        textField.resignFirstResponder()
        if textField === Username {
            // When the user finished with the Username, pass to the Password
            Password.becomeFirstResponder()
        } else if textField === Password {
            // When the user finished also with the Password, hide the keyboard
            Password.resignFirstResponder()
        }
        return true
    }
    
    // MARK: Behaviour after login attempt
    
    func PostAuthFail() {
        // If authentication is unsuccessful then alert the user
        self.HighlightEmptyFields()
        
        // Stop animating the activity indicator
        self.LoginActivity.stopAnimating()
    }
    
    func PostLoginSuccess() {
        // Stop animating activity indicator
        self.LoginActivity.stopAnimating()
        
        // Save the authenticated user info
        appDelegate.serverDelegate.saveState()
        
        // Pass to the following view
        self.performSegueWithIdentifier("LoggedInSegue", sender: self)
    }
}

