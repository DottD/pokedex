import Foundation
import PGoApi
import CoreLocation
import UIKit

class PokemonViewController : UICollectionViewController, CLLocationManagerDelegate {
    
    // MARK: Properties
    @IBOutlet weak var MenuButton: PokeButton!
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var startAppBanner: STABannerView?
    
    // MARK: Convenience methods
    private func getPokemonDataArray(fromSectionIndex section: Int) -> [PokemonData]? {
        // Sort the Pokemon dictionary by keys
        let sortedKeys = appDelegate.serverDelegate.Pokemon.keys.sort() { $0 < $1 }
        var gen = sortedKeys.generate()
        for idx in 0...section { // gen.next() gives the first element at first use
            if let key = gen.next() {
                if idx == section {
                    return appDelegate.serverDelegate.Pokemon[key]
                }
            } else {
                return nil
            }
        }
        return nil
    }
    
    // MARK: Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check existence of superview controller SWRevealViewController
        if self.revealViewController() != nil {
            // Link MenuButton with the menu opening
            self.MenuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), forControlEvents: UIControlEvents.TouchDown)
            // Add a left-to-right swipe recognizer to open the menu
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            // Add a tap gesture recognizer to close the menu
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        // Check presence of data in serverDelegate
        if !appDelegate.serverDelegate.hasData(ofType: .Pokemon) {
            appDelegate.serverDelegate.PostInventorySuccess = {
                self.collectionView?.reloadData()
            }
            appDelegate.serverDelegate.askServer(request: .InventoryAndPokemon)
        }
        
        // Initialize the "pull to refresh" control
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = AppDelegate.textColor
        refreshControl.addTarget(self, action: #selector(InventoryViewController.RefreshByPull), forControlEvents: UIControlEvents.ValueChanged)
        self.collectionView?.alwaysBounceVertical = true;
        self.collectionView?.addSubview(refreshControl)
        
        // Setup StartApp Banner
        if (startAppBanner == nil) {
            startAppBanner = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Bottom, withView: self.view, withDelegate: nil);
            self.view.addSubview(startAppBanner!)
        }
        
        showUpdatedDate()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Display and organize pokemons
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getPokemonDataArray(fromSectionIndex: section)!.count
    }
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return appDelegate.serverDelegate.Pokemon.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        // Load the prototype
        let NewCell = collectionView.dequeueReusableCellWithReuseIdentifier("PokemonCell", forIndexPath: indexPath) as! PokemonCollectionViewCell
        
        // Look up in the Pokemon list in the serverDelegate
        // Remember that section index is among the non-nil items of pokemon list
        if let PokemonSection = getPokemonDataArray(fromSectionIndex: indexPath.section) {
            let Pokemon = PokemonSection[indexPath.row]
            
            // Get pokemon name
            if let NickName = Pokemon.getNickName() {
                NewCell.Name.text = NickName
            } else {
                if let PokemonId = Pokemon.getPokemonId() {
                    NewCell.Name.text = PokemonId.toString().capitalizedString
                }
            }
            // Get pokemon stats
            if let Attack = Pokemon.getIndividualAttack() {
                if let Defense = Pokemon.getIndividualDefense() {
                    if let Stamina = Pokemon.getIndividualStamina() {
                        NewCell.Sprite.updateIV(toAttack: Attack, toDefense: Defense, toStamina: Stamina)
                        let OverallScore = Int( CGFloat(Attack + Defense + Stamina)/(3.0*AppDelegate.MaxIV)*100.0 )
                        NewCell.IV.text = String(OverallScore)+" %"
                    }
                }
            }
            
            if let CP = Pokemon.getCP() {
                NewCell.CP.text = String(CP)
            }
            NewCell.Sprite.setNeedsDisplay()
        } else {
            NewCell.Name.text = "Error"
        }
        
        return NewCell
    }
    
    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        // Create an instance of PokemonHeaderReusableView
        let NewHeader = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "PokemonHeader", forIndexPath: indexPath) as! PokemonHeaderReusableView
        
        // Look up in the Pokemon list in the serverDelegate
        // Remember that section index is among the non-nil items of pokemon list
        if let PokemonSection = getPokemonDataArray(fromSectionIndex: indexPath.section) {
            let Pokemon = PokemonSection[indexPath.row]
            if let PokemonId = Pokemon.getPokemonId() {
                let PokemonString = PokemonId.toString()
                NewHeader.PokemonName.text = PokemonString.capitalizedString
                NewHeader.PokemonSprite.image = UIImage(named: PokemonString.lowercaseString)
            }
        } else {
            NewHeader.PokemonName.text = "Error"
        }
        
        return NewHeader
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("PokemonSpecsSegue", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let NextViewController = segue.destinationViewController as? PokemonSpecsController {
            // Pass the selected item to the following view
            let SelectedItem = collectionView!.indexPathsForSelectedItems()?.last
            NextViewController.IndexPath = SelectedItem!
            NextViewController.Pokemon = getPokemonDataArray(fromSectionIndex: SelectedItem!.section)![SelectedItem!.row]
        }
    }
    
    // Show the last update date
    func showUpdatedDate() {
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        let textualDate = "Last Update: " + dateFormatter.stringFromDate(appDelegate.serverDelegate.UpdatedDate) as NSString
        let font = UIFont(name: "Helvetica", size: 12.0)!
        let size = textualDate.sizeWithAttributes(["NSFontAttributeName":font])
        let rect = CGRect(x: -size.width/2, y: 0, width: size.width, height: size.height)
        let label = UILabel(frame: rect)
        label.textColor = AppDelegate.textColor
        label.font = font
        label.text = String(textualDate)
        self.navigationItem.setRightBarButtonItem(UIBarButtonItem(customView: label), animated: false)
    }
    
    // Make another request to the server to refresh the list
    func RefreshByPull() {
        // Ask to the server
        appDelegate.serverDelegate.PostInventorySuccess = {
            // Reload the table
            self.collectionView!.reloadData()
            
            // End refreshing
            for view in (self.collectionView?.subviews)! {
                if let refreshControl = view as? UIRefreshControl {
                    refreshControl.endRefreshing()
                }
            }
            
            // Show the update date
            self.showUpdatedDate()
        }
        appDelegate.serverDelegate.askServer(request: .InventoryAndPokemon)
    }
}