//
//  PogoExt.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 13/09/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import PGoApi

class InventoryData: NSObject, NSCoding {
    // MARK: Properties
    private var Id: Int32?
    private var Count: Int32?
    private var Unseen: Bool?
    
    private enum Field: String {
        case Id
        case Count
        case Unseen
    }
    
    // MARK: Convenience initializations method
    init(fromItemData data: Pogoprotos.Inventory.Item.ItemData) {
        if data.hasItemId {
            self.Id = data.itemId.rawValue
        } else {
            self.Id = nil
        }
        if data.hasCount {
            self.Count = data.count
        } else {
            self.Count = nil
        }
        if data.hasUnseen {
            self.Unseen = data.unseen
        } else {
            self.Unseen = nil
        }
    }
    
    // MARK: Encoding and decoding procedures
    func encodeWithCoder(aCoder: NSCoder) {
        if let Id = self.Id {
            aCoder.encodeInt32(Id, forKey: Field.Id.rawValue)
        }
        if let Count = self.Count {
            aCoder.encodeInt32(Count, forKey: Field.Count.rawValue)
        }
        if let Unseen = self.Unseen {
            aCoder.encodeBool(Unseen, forKey: Field.Unseen.rawValue)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        self.Id = aDecoder.decodeInt32ForKey(Field.Id.rawValue)
        self.Count = aDecoder.decodeInt32ForKey(Field.Count.rawValue)
        self.Unseen = aDecoder.decodeBoolForKey(Field.Unseen.rawValue)
    }
    
    // MARK: Getters for private properties
    func getId() -> Pogoprotos.Inventory.Item.ItemId? {
        if self.Id != nil {
            return Pogoprotos.Inventory.Item.ItemId(rawValue: self.Id!)
        } else {
            return nil
        }
    }
    func getCount() -> Int? {
        if self.Count != nil {
            return Int(self.Count!)
        } else {
            return nil
        }
    }
    func getUnseen() -> Bool? {
        return self.Unseen
    }
    
    // MARK: Getters for private properties
    func setId(toValue val: Pogoprotos.Inventory.Item.ItemId) {
        self.Id = val.rawValue
    }
    func setCount(toValue val: Int) {
        self.Count = Int32(val)
    }
    func setUnseen(toValue val: Bool) {
        self.Unseen = val
    }
}

class PokemonData: NSObject, NSCoding {
    // MARK: Properties
    private var ID: UInt64?
    private var NickName: String?
    private var PokemonId: Int32?
    private var IndividualAttack: Int32?
    private var IndividualDefense: Int32?
    private var IndividualStamina: Int32?
    private var CP: Int32?
    private var Move1: Int32?
    private var Move2: Int32?
    private var Height: Float?
    private var Weight: Float?
    private var IsEgg: Bool?
    private var Origin: Int32?
    private var Favorite: Int32?
    private var Stamina: Int32?
    private var StaminaMax: Int32?
    private var FromFort: Int32?
    private var NumUpgrades: Int32?
    private var BattlesAttacked: Int32?
    private var BattlesDefended: Int32?
    private var BuddyCandyAwarded: Int32?
    private var Pokeball: Int32?
    private var CpMultiplier: Float?
    private var AdditionalCpMultiplier: Float?
    private var CapturedCellId: UInt64?
    private var CreationTimeMs: UInt64?
    private var OwnerName: String?
    private var DeployedFortId: String?
    private var EggIncubatorId: String?
    private var EggKmWalkedStart: Double?
    private var EggKmWalkedTarget: Double?
    
    private enum Field: String {
        case ID
        case NickName
        case PokemonId
        case IndividualAttack
        case IndividualDefense
        case IndividualStamina
        case CP
        case Move1
        case Move2
        case Height
        case Weight
        case IsEgg
        case Origin
        case Favorite
        case Stamina
        case StaminaMax
        case FromFort
        case NumUpgrades
        case BattlesAttacked
        case BattlesDefended
        case BuddyCandyAwarded
        case Pokeball
        case CpMultiplier
        case AdditionalCpMultiplier
        case CapturedCellId
        case CreationTimeMs
        case OwnerName
        case DeployedFortId
        case EggIncubatorId
        case EggKmWalkedStart
        case EggKmWalkedTarget
    }
    
    // MARK: Convenience initializations method
    override init() {
        
    }
    init(fromPokemonData data: Pogoprotos.Data.PokemonData) {
        if data.hasId {
            self.ID = data.id
        }
        if data.hasNickname {
            self.NickName = data.nickname
        }
        if data.hasPokemonId {
            self.PokemonId = data.pokemonId.rawValue
        }
        if data.hasIndividualAttack {
            self.IndividualAttack = data.individualAttack
        }
        if data.hasIndividualDefense {
            self.IndividualDefense = data.individualDefense
        }
        if data.hasIndividualStamina {
            self.IndividualStamina = data.individualStamina
        }
        if data.hasCp {
            self.CP = data.cp
        }
        if data.hasMove1 {
            self.Move1 = data.move1.rawValue
        }
        if data.hasMove2 {
            self.Move2 = data.move2.rawValue
        }
        if data.hasHeightM {
            self.Height = data.heightM
        }
        if data.hasWeightKg {
            self.Weight = data.weightKg
        }
        if data.hasIsEgg {
            self.IsEgg = data.isEgg
        }
        if data.hasOrigin {
            self.Origin = data.origin
        }
        if data.hasFavorite {
            self.Favorite = data.favorite
        }
        if data.hasStamina {
            self.Stamina = data.stamina
        }
        if data.hasStaminaMax {
            self.StaminaMax = data.staminaMax
        }
        if data.hasFromFort {
            self.FromFort = data.fromFort
        }
        if data.hasNumUpgrades {
            self.NumUpgrades = data.numUpgrades
        }
        if data.hasBattlesAttacked {
            self.BattlesAttacked = data.battlesAttacked
        }
        if data.hasBattlesDefended {
            self.BattlesDefended = data.battlesDefended
        }
        if data.hasBuddyCandyAwarded {
            self.BuddyCandyAwarded = data.buddyCandyAwarded
        }
        if data.hasPokeball {
            self.Pokeball = data.pokeball.rawValue
        }
        if data.hasCpMultiplier {
            self.CpMultiplier = data.cpMultiplier
        }
        if data.hasAdditionalCpMultiplier {
            self.AdditionalCpMultiplier = data.additionalCpMultiplier
        }
        if data.hasCapturedCellId {
            self.CapturedCellId = data.capturedCellId
        }
        if data.hasCreationTimeMs {
            self.CreationTimeMs = data.creationTimeMs
        }
        if data.hasOwnerName {
            self.OwnerName = data.ownerName
        }
        if data.hasDeployedFortId {
            self.DeployedFortId = data.deployedFortId
        }
        if data.hasEggIncubatorId {
            self.EggIncubatorId = data.eggIncubatorId
        }
        if data.hasEggKmWalkedStart {
            self.EggKmWalkedStart = data.eggKmWalkedStart
        }
        if data.hasEggKmWalkedTarget {
            self.EggKmWalkedTarget = data.eggKmWalkedTarget
        }
    }
    
    // MARK: Encoding and decoding procedures
    func encodeWithCoder(aCoder: NSCoder) {
        if let ID = self.ID {
            if ID != UInt64(bitPattern: Int64(bitPattern: ID)) {
                fatalError("Impossible to use encodeInt64")
            }
            aCoder.encodeInt64(Int64(bitPattern: ID), forKey: Field.ID.rawValue)
        }
        if let NickName = self.NickName {
            aCoder.encodeObject(NickName, forKey: Field.NickName.rawValue)
        }
        if let PokemonId = self.PokemonId {
            aCoder.encodeInt32(PokemonId, forKey: Field.PokemonId.rawValue)
        }
        if let Attack = self.IndividualAttack {
            aCoder.encodeInt32(Attack, forKey: Field.IndividualAttack.rawValue)
        }
        if let Defense = self.IndividualDefense {
            aCoder.encodeInt32(Defense, forKey: Field.IndividualDefense.rawValue)
        }
        if let Stamina = self.IndividualStamina {
            aCoder.encodeInt32(Stamina, forKey: Field.IndividualStamina.rawValue)
        }
        if let CP = self.CP {
            aCoder.encodeInt32(CP, forKey: Field.CP.rawValue)
        }
        if let Move1 = self.Move1 {
            aCoder.encodeInt32(Move1, forKey: Field.Move1.rawValue)
        }
        if let Move2 = self.Move2 {
            aCoder.encodeInt32(Move2
                , forKey: Field.Move2.rawValue)
        }
        if let Height = self.Height {
            aCoder.encodeFloat(Height, forKey: Field.Height.rawValue)
        }
        if let Weight = self.Weight {
            aCoder.encodeFloat(Weight, forKey: Field.Weight.rawValue)
        }
        if let IsEgg = self.IsEgg {
            aCoder.encodeBool(IsEgg, forKey: Field.IsEgg.rawValue)
        }
        if let Origin = self.Origin {
            aCoder.encodeInt32(Origin, forKey: Field.Origin.rawValue)
        }
        if let Favorite = self.Favorite {
            aCoder.encodeInt32(Favorite, forKey: Field.Favorite.rawValue)
        }
        if let Stamina = self.Stamina {
            aCoder.encodeInt32(Stamina, forKey: Field.Stamina.rawValue)
        }
        if let StaminaMax = self.StaminaMax {
            aCoder.encodeInt32(StaminaMax, forKey: Field.StaminaMax.rawValue)
        }
        if let FromFort = self.FromFort {
            aCoder.encodeInt32(FromFort, forKey: Field.FromFort.rawValue)
        }
        if let NumUpgrades = self.NumUpgrades {
            aCoder.encodeInt32(NumUpgrades, forKey: Field.NumUpgrades.rawValue)
        }
        if let BattlesAttacked = self.BattlesAttacked {
            aCoder.encodeInt32(BattlesAttacked, forKey: Field.BattlesAttacked.rawValue)
        }
        if let BattlesDefended = self.BattlesDefended {
            aCoder.encodeInt32(BattlesDefended, forKey: Field.BattlesDefended.rawValue)
        }
        if let BuddyCandyAwarded = self.BuddyCandyAwarded {
            aCoder.encodeInt32(BuddyCandyAwarded, forKey: Field.BuddyCandyAwarded.rawValue)
        }
        if let Pokeball = self.Pokeball {
            aCoder.encodeInt32(Pokeball, forKey: Field.Pokeball.rawValue)
        }
        if let CpMultiplier = self.CpMultiplier {
            aCoder.encodeFloat(CpMultiplier, forKey: Field.CpMultiplier.rawValue)
        }
        if let AdditionalCpMultiplier = self.AdditionalCpMultiplier {
            aCoder.encodeFloat(AdditionalCpMultiplier, forKey: Field.AdditionalCpMultiplier.rawValue)
        }
        if let CapturedCellId = self.CapturedCellId {
            aCoder.encodeInt64(Int64(bitPattern: CapturedCellId), forKey: Field.CapturedCellId.rawValue)
        }
        if let CreationTimeMs = self.CreationTimeMs {
            aCoder.encodeInt64(Int64(bitPattern: CreationTimeMs), forKey: Field.CreationTimeMs.rawValue)
        }
        if let OwnerName = self.OwnerName {
            aCoder.encodeObject(OwnerName, forKey: Field.OwnerName.rawValue)
        }
        if let DeployedFortId = self.DeployedFortId {
            aCoder.encodeObject(DeployedFortId, forKey: Field.DeployedFortId.rawValue)
        }
        if let EggIncubatorId = self.EggIncubatorId {
            aCoder.encodeObject(EggIncubatorId, forKey: Field.EggIncubatorId.rawValue)
        }
        if let AdditionalCpMultiplier = self.EggKmWalkedStart {
            aCoder.encodeDouble(AdditionalCpMultiplier, forKey: Field.EggKmWalkedStart.rawValue)
        }
        if let AdditionalCpMultiplier = self.EggKmWalkedTarget {
            aCoder.encodeDouble(AdditionalCpMultiplier, forKey: Field.EggKmWalkedTarget.rawValue)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        self.ID = UInt64(bitPattern: aDecoder.decodeInt64ForKey(Field.ID.rawValue))
        self.NickName = aDecoder.decodeObjectForKey(Field.NickName.rawValue) as? String
        self.PokemonId = aDecoder.decodeInt32ForKey(Field.PokemonId.rawValue)
        self.IndividualAttack = aDecoder.decodeInt32ForKey(Field.IndividualAttack.rawValue)
        self.IndividualDefense = aDecoder.decodeInt32ForKey(Field.IndividualDefense.rawValue)
        self.IndividualStamina = aDecoder.decodeInt32ForKey(Field.IndividualStamina.rawValue)
        self.CP = aDecoder.decodeInt32ForKey(Field.CP.rawValue)
        self.Move1 = aDecoder.decodeInt32ForKey(Field.Move1.rawValue)
        self.Move2 = aDecoder.decodeInt32ForKey(Field.Move2.rawValue)
        self.Height = aDecoder.decodeFloatForKey(Field.Height.rawValue)
        self.Weight = aDecoder.decodeFloatForKey(Field.Weight.rawValue)
        self.IsEgg = aDecoder.decodeBoolForKey(Field.IsEgg.rawValue)
        self.Origin = aDecoder.decodeInt32ForKey(Field.Origin.rawValue)
        self.Favorite = aDecoder.decodeInt32ForKey(Field.Favorite.rawValue)
        self.Stamina = aDecoder.decodeInt32ForKey(Field.Stamina.rawValue)
        self.StaminaMax = aDecoder.decodeInt32ForKey(Field.StaminaMax.rawValue)
        self.FromFort = aDecoder.decodeInt32ForKey(Field.FromFort.rawValue)
        self.NumUpgrades = aDecoder.decodeInt32ForKey(Field.NumUpgrades.rawValue)
        self.BattlesAttacked = aDecoder.decodeInt32ForKey(Field.BattlesAttacked.rawValue)
        self.BattlesDefended = aDecoder.decodeInt32ForKey(Field.BattlesDefended.rawValue)
        self.BuddyCandyAwarded = aDecoder.decodeInt32ForKey(Field.BuddyCandyAwarded.rawValue)
        self.Pokeball = aDecoder.decodeInt32ForKey(Field.Pokeball.rawValue)
        self.CpMultiplier = aDecoder.decodeFloatForKey(Field.CpMultiplier.rawValue)
        self.AdditionalCpMultiplier = aDecoder.decodeFloatForKey(Field.AdditionalCpMultiplier.rawValue)
        self.CapturedCellId = UInt64(bitPattern: aDecoder.decodeInt64ForKey(Field.CapturedCellId.rawValue))
        self.CreationTimeMs = UInt64(bitPattern: aDecoder.decodeInt64ForKey(Field.CreationTimeMs.rawValue))
        self.OwnerName = aDecoder.decodeObjectForKey(Field.OwnerName.rawValue) as? String
        self.DeployedFortId = aDecoder.decodeObjectForKey(Field.DeployedFortId.rawValue) as? String
        self.EggIncubatorId = aDecoder.decodeObjectForKey(Field.EggIncubatorId.rawValue) as? String
        self.EggKmWalkedStart = aDecoder.decodeDoubleForKey(Field.EggKmWalkedStart.rawValue)
        self.EggKmWalkedTarget = aDecoder.decodeDoubleForKey(Field.EggKmWalkedTarget.rawValue)
    }
    
    // MARK: Getters for private properties
    func getID() -> UInt64? {
        return self.ID
    }
    func getNickName() -> String? {
        if self.NickName != nil {
            return self.NickName
        } else {
            return nil
        }
    }
    func getPokemonId() -> Pogoprotos.Enums.PokemonId? {
        if let PokemonId = self.PokemonId {
            return Pogoprotos.Enums.PokemonId(rawValue: PokemonId)
        } else {
            return nil
        }
    }
    func getIndividualAttack() -> Int? {
        if self.IndividualAttack != nil {
            return Int(self.IndividualAttack!)
        } else {
            return nil
        }
    }
    func getIndividualDefense() -> Int? {
        if self.IndividualDefense != nil {
            return Int(self.IndividualDefense!)
        } else {
            return nil
        }
    }
    func getIndividualStamina() -> Int? {
        if self.IndividualStamina != nil {
            return Int(self.IndividualStamina!)
        } else {
            return nil
        }
    }
    func getCP() -> Int? {
        if self.CP != nil {
            return Int(self.CP!)
        } else {
            return nil
        }
    }
    func getMove1() -> Pogoprotos.Enums.PokemonMove? {
        if self.Move1 != nil {
            return Pogoprotos.Enums.PokemonMove(rawValue: self.Move1!)
        } else {
            return nil
        }
    }
    func getMove2() -> Pogoprotos.Enums.PokemonMove? {
        if self.Move2 != nil {
            return Pogoprotos.Enums.PokemonMove(rawValue: self.Move2!)
        } else {
            return nil
        }
    }
    func getHeight() -> Float? {
        return self.Height
    }
    func getWeight() -> Float? {
        return self.Weight
    }
    func getIsEgg() -> Bool? {
        return self.IsEgg
    }
    func getOrigin() -> Int? {
        if let Origin = self.Origin {
            return Int(Origin)
        } else {
            return nil
        }
    }
    func getFavorite() -> Int? {
        if let Favorite = self.Favorite {
            return Int(Favorite)
        } else {
            return nil
        }
    }
    func getStamina() -> Int? {
        if let Stamina = self.Stamina {
            return Int(Stamina)
        } else {
            return nil
        }
    }
    func getStaminaMax() -> Int? {
        if let StaminaMax = self.StaminaMax {
            return Int(StaminaMax)
        } else {
            return nil
        }
    }
    func getFromFort() -> Int? {
        if let FromFort = self.FromFort {
            return Int(FromFort)
        } else {
            return nil
        }
    }
    func getNumUpgrades() -> Int? {
        if let NumUpgrades = self.NumUpgrades {
            return Int(NumUpgrades)
        } else {
            return nil
        }
    }
    func getBattlesAttacked() -> Int? {
        if let BattlesAttacked = self.BattlesAttacked {
            return Int(BattlesAttacked)
        } else {
            return nil
        }
    }
    func getBattlesDefended() -> Int? {
        if let BattlesDefended = self.BattlesDefended {
            return Int(BattlesDefended)
        } else {
            return nil
        }
    }
    func getBuddyCandyAwarded() -> Int? {
        if let BuddyCandyAwarded = self.BuddyCandyAwarded {
            return Int(BuddyCandyAwarded)
        } else {
            return nil
        }
    }
    func getPokeball() -> Pogoprotos.Inventory.Item.ItemId? {
        if let Pokeball = self.Pokeball {
            return Pogoprotos.Inventory.Item.ItemId(rawValue: Pokeball)
        } else {
            return nil
        }
    }
    func getCpMultiplier() -> Float? {
        return self.CpMultiplier
    }
    func getAdditionalCpMultiplier() -> Float? {
        return self.AdditionalCpMultiplier
    }
    func getCapturedCellId() -> UInt64? {
        return self.CapturedCellId
    }
    func getCreationTimeMs() -> UInt64? {
        return self.CreationTimeMs
    }
    func getOwnerName() -> String? {
        return self.OwnerName
    }
    func getDeployedFortId() -> String? {
        return self.DeployedFortId
    }
    func getEggIncubatorId() -> String? {
        return self.EggIncubatorId
    }
    func getEggKmWalkedStart() -> Double? {
        return self.EggKmWalkedStart
    }
    func getEggKmWalkedTarget() -> Double? {
        return self.EggKmWalkedTarget
    }
    
    // MARK: Setters for private properties
    func setID(toValue val: UInt64) {
        self.ID = val
    }
    func setNickName(toValue val: String) {
        self.NickName = val
    }
    func setPokemonId(toValue val: Pogoprotos.Enums.PokemonId) {
        self.PokemonId = val.rawValue
    }
    func setIndividualAttack(toValue val: Int) {
        self.IndividualAttack = Int32(val)
    }
    func setIndividualDefense(toValue val: Int) {
        self.IndividualDefense = Int32(val)
    }
    func setIndividualStamina(toValue val: Int) {
        self.IndividualStamina = Int32(val)
    }
    func setCP(toValue val: Int) {
        self.CP = Int32(val)
    }
    func setMove1(toValue val: Pogoprotos.Enums.PokemonMove) {
        self.Move1 = val.rawValue
    }
    func setMove2(toValue val: Pogoprotos.Enums.PokemonMove) {
        self.Move2 = val.rawValue
    }
    func setHeight(toValue val: Float) {
        self.Height = val
    }
    func setWeight(toValue val: Float) {
        self.Weight = val
    }
    func setIsEgg(toValue val: Bool) {
        self.IsEgg = val
    }
    func setOrigin(toValue val: Int) {
        self.Origin = Int32(val)
    }
    func setFavorite(toValue val: Int) {
        self.Favorite = Int32(val)
    }
    func setStamina(toValue val: Int) {
        self.Stamina = Int32(val)
    }
    func setStaminaMax(toValue val: Int) {
        self.StaminaMax = Int32(val)
    }
    func setFromFort(toValue val: Int) {
        self.FromFort = Int32(val)
    }
    func setNumUpgrades(toValue val: Int) {
        self.NumUpgrades = Int32(val)
    }
    func setBattlesAttacked(toValue val: Int) {
        self.BattlesAttacked = Int32(val)
    }
    func setBattlesDefended(toValue val: Int) {
        self.BattlesDefended = Int32(val)
    }
    func setBuddyCandyAwarded(toValue val: Int) {
        self.BuddyCandyAwarded = Int32(val)
    }
    func setPokeball(toValue val: Pogoprotos.Inventory.Item.ItemId) {
        self.Pokeball = val.rawValue
    }
    func setCpMultiplier(toValue val: Float) {
        self.CpMultiplier = val
    }
    func setAdditionalCpMultiplier(toValue val: Float) {
        self.AdditionalCpMultiplier = val
    }
    func setCapturedCellId(toValue val: UInt64) {
        self.CapturedCellId = val
    }
    func setCreationTimeMs(toValue val: UInt64) {
        self.CreationTimeMs = val
    }
    func setOwnerName(toValue val: String) {
        self.OwnerName = val
    }
    func setDeployedFortId(toValue val: String) {
        self.DeployedFortId = val
    }
    func setEggIncubatorId(toValue val: String) {
        self.EggIncubatorId = val
    }
    func setEggKmWalkedStart(toValue val: Double) {
        self.EggKmWalkedStart = val
    }
    func setEggKmWalkedTarget(toValue val: Double) {
        self.EggKmWalkedTarget = val
    }
}

class PokedexData: NSObject, NSCoding {
    // MARK: Properties
    private var PokemonId: Int32?
    private var TimesCaptured: Int32?
    private var TimesEncountered: Int32?
    private var EvolutionStones: Int32?
    private var EvolutionStonePieces: Int32?
    
    private enum Field: String {
        case PokemonId
        case TimesCaptured
        case TimesEncountered
        case EvolutionStones
        case EvolutionStonePieces
    }
    
    // MARK: Convenience initializations method
    init(fromPokedexEntry data: Pogoprotos.Data.PokedexEntry) {
        if data.hasPokemonId {
            self.PokemonId = data.pokemonId.rawValue
        } else {
            self.PokemonId = nil
        }
        if data.hasTimesCaptured {
            self.TimesCaptured = data.timesCaptured
        } else {
            self.TimesCaptured = nil
        }
        if data.hasTimesEncountered {
            self.TimesEncountered = data.timesEncountered
        } else {
            self.TimesEncountered = nil
        }
        if data.hasEvolutionStones {
            self.EvolutionStones = data.evolutionStones
        } else {
            self.EvolutionStones = nil
        }
        if data.hasEvolutionStonePieces {
            self.EvolutionStonePieces = data.evolutionStonePieces
        } else {
            self.EvolutionStonePieces = nil
        }
    }
    
    // MARK: Encoding and decoding procedures
    func encodeWithCoder(aCoder: NSCoder) {
        if let PokemonId = self.PokemonId {
            aCoder.encodeInt32(PokemonId, forKey: Field.PokemonId.rawValue)
        }
        if let TimesCaptured = self.TimesCaptured {
            aCoder.encodeInt32(TimesCaptured, forKey: Field.TimesCaptured.rawValue)
        }
        if let TimesEncountered = self.TimesEncountered {
            aCoder.encodeInt32(TimesEncountered, forKey: Field.TimesEncountered.rawValue)
        }
        if let EvolutionStones = self.EvolutionStones {
            aCoder.encodeInt32(EvolutionStones, forKey: Field.EvolutionStones.rawValue)
        }
        if let EvolutionStonePieces = self.EvolutionStonePieces {
            aCoder.encodeInt32(EvolutionStonePieces, forKey: Field.EvolutionStonePieces.rawValue)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        self.PokemonId = aDecoder.decodeInt32ForKey(Field.PokemonId.rawValue)
        self.TimesCaptured = aDecoder.decodeInt32ForKey(Field.TimesCaptured.rawValue)
        self.TimesEncountered = aDecoder.decodeInt32ForKey(Field.TimesEncountered.rawValue)
        self.EvolutionStones = aDecoder.decodeInt32ForKey(Field.EvolutionStones.rawValue)
        self.EvolutionStonePieces = aDecoder.decodeInt32ForKey(Field.EvolutionStonePieces.rawValue)
    }
    
    // MARK: Getters for private properties
    func getPokemonId() -> Pogoprotos.Enums.PokemonId? {
        if self.PokemonId != nil {
            return Pogoprotos.Enums.PokemonId(rawValue: self.PokemonId!)
        } else {
            return nil
        }
    }
    func getTimesCaptured() -> Int? {
        if self.TimesCaptured != nil {
            return Int(self.TimesCaptured!)
        } else {
            return nil
        }
    }
    func getTimesEncountered() -> Int? {
        if self.TimesEncountered != nil {
            return Int(self.TimesEncountered!)
        } else {
            return nil
        }
    }
    func getEvolutionStones() -> Int? {
        if self.EvolutionStones != nil {
            return Int(self.EvolutionStones!)
        } else {
            return nil
        }
    }
    func getEvolutionStonePieces() -> Int? {
        if self.EvolutionStonePieces != nil {
            return Int(self.EvolutionStonePieces!)
        } else {
            return nil
        }
    }
    
    // MARK: Setters for private properties
    func setPokemonId(toValue val: Pogoprotos.Enums.PokemonId) {
        self.PokemonId = val.rawValue
    }
    func setTimesCaptured(toValue val: Int) {
        self.PokemonId = Int32(val)
    }
    func setTimesEncountered(toValue val: Int) {
        self.PokemonId = Int32(val)
    }
    func setEvolutionStones(toValue val: Int) {
        self.PokemonId = Int32(val)
    }
    func setEvolutionStonePieces(toValue val: Int) {
        self.PokemonId = Int32(val)
    }
}

class CandiesData: NSObject, NSCoding {
    // MARK: Properties
    private var FamilyId: Int32?
    private var Candy: Int32?
    
    private enum Field: String {
        case FamilyId
        case Candy
    }
    
    // MARK: Convenience initializations method
    init(fromCandy data: Pogoprotos.Inventory.Candy) {
        if data.hasFamilyId {
            self.FamilyId = data.familyId.rawValue
        } else {
            self.FamilyId = nil
        }
        if data.hasCandy {
            self.Candy = data.candy
        } else {
            self.Candy = nil
        }
    }
    
    // MARK: Encoding and decoding procedures
    func encodeWithCoder(aCoder: NSCoder) {
        if let FamilyId = self.FamilyId {
            aCoder.encodeInt32(FamilyId, forKey: Field.FamilyId.rawValue)
        }
        if let Candy = self.Candy {
            aCoder.encodeInt32(Candy, forKey: Field.Candy.rawValue)
        }
    }
    required init?(coder aDecoder: NSCoder) {
        self.FamilyId = aDecoder.decodeInt32ForKey(Field.FamilyId.rawValue)
        self.Candy = aDecoder.decodeInt32ForKey(Field.Candy.rawValue)
    }
    
    // MARK: Getters for private properties
    func getFamilyId() -> Pogoprotos.Enums.PokemonFamilyId? {
        if self.FamilyId != nil {
            return Pogoprotos.Enums.PokemonFamilyId(rawValue: self.FamilyId!)
        } else {
            return nil
        }
    }
    func getCandy() -> Int? {
        if self.Candy != nil {
            return Int(self.Candy!)
        } else {
            return nil
        }
    }
    
    // MARK: Setters for private properties
    func getFamilyId(toValue val: Pogoprotos.Enums.PokemonFamilyId) {
        self.FamilyId = val.rawValue
    }
    func getCandy(toValue val: Int) {
        self.Candy = Int32(val)
    }
}