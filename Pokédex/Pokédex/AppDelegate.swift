//
//  AppDelegate.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 29/07/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import UIKit
import CoreLocation
import PGoApi

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // General config
    static let MaxIV : CGFloat = 15.0
    static let highlightColor = UIColor(red: 216.0/255.0, green: 71.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    static var normalColor = UIColor.clearColor()
    static let textColor = UIColor(red: 253/255, green: 177/255, blue: 75/255, alpha: 1.0)
    // Login config
    var serverDelegate = ServerDelegate()
    
    static func PopupMessage(withTitle title: String, withMessage message: String, fromCaller Caller: AnyObject) {
        let PopupError = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        PopupError.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        Caller.presentViewController(PopupError, animated: true, completion: nil)
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        // initialize the SDK with your appID and devID
        let sdk: STAStartAppSDK = STAStartAppSDK.sharedInstance()
        sdk.appID = "Your App Id"
        sdk.devID = "108849633"
//        sdk.preferences = STASDKPreferences.prefrencesWithAge(age: UInt, andGender: STAGender)
        let pref = STASplashPreferences()
        pref.isSplashLoadingIndicatorEnabled = false
        pref.splashAdDisplayTime = STASplashAdDisplayTimeLong
        pref.splashMode = STASplashModeTemplate
        pref.splashTemplateTheme = STASplashTemplateThemeGloomy
        sdk.showSplashAdWithPreferences(pref)
        
        let GotoRevealView = {
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
            let story = UIStoryboard(name: "Main", bundle: nil)
            let controller = story.instantiateViewControllerWithIdentifier("RevealViewController")
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
        let GotoLoginView = {
            self.window = UIWindow.init(frame: UIScreen.mainScreen().bounds)
            let story = UIStoryboard(name: "Main", bundle: nil)
            let controller = story.instantiateViewControllerWithIdentifier("LoginViewController")
            self.window?.rootViewController = controller
            self.window?.makeKeyAndVisible()
        }
        // Try to load data from file
        if serverDelegate.loadState() {
            // Check if there is the main user registered
            if serverDelegate.isUserRegistered(withPurpose: .Main) {
                // If the user is registered, he must authenticate
                serverDelegate.PostAuthSuccess = {
                    // In the case of successfull authentication check the existence of data
                    if self.serverDelegate.hasData(ofType: .All) {
                        // If it has data go to pokedex view
                        GotoRevealView()
                    } else {
                        // Otherwise ask the server for new data and go to pokedex view
                        self.serverDelegate.PostInventorySuccess = {
                            self.serverDelegate.saveState()
                            GotoRevealView()
                        }
                        self.serverDelegate.askServer(request: .InventoryAndPokemon)
                    }
                }
                serverDelegate.PostAuthFail = {
                    // If authentication is unsuccessful go to login view to ask for new credentials
                    GotoLoginView()
                }
                serverDelegate
                    .authenticate(withPurpose: .Main)
            } else {
                // If not, go to login view
                GotoLoginView()
            }
        } else {
            // If not, go to login view
            GotoLoginView()
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

