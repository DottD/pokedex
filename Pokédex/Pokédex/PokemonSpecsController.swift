//
//  PokemonSpecsController.swift
//  Pokédex
//
//  Created by Filippo Santarelli on 26/08/16.
//  Copyright © 2016 Filippo Santarelli. All rights reserved.
//

import Foundation
import PGoApi

class PokemonSpecsController: UIViewController {
    
    var Pokemon = PokemonData()
    var IndexPath = NSIndexPath(forItem: 0, inSection: 0)
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet weak var Sprite: UIImageView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var CP: UILabel!
    @IBOutlet weak var HP: UILabel!
    @IBOutlet weak var Candy: UILabel!
    @IBOutlet weak var Type: UILabel!
    @IBOutlet weak var Weight: UILabel!
    @IBOutlet weak var Height: UILabel!
    @IBOutlet weak var MinorAttackName: UILabel!
    @IBOutlet weak var MinorAttackStrength: UILabel!
    @IBOutlet weak var MajorAttackName: UILabel!
    @IBOutlet weak var MajorAttackStrength: UILabel!
    @IBOutlet weak var IV: PieIVView!
    @IBOutlet weak var IVperc: UILabel!
    @IBOutlet weak var TODOScheme: UISegmentedControl!
    
    @IBAction func ChangedTODOScheme(sender: AnyObject) {
        var NewName : String = ""
        switch self.TODOScheme.selectedSegmentIndex {
        case 0: // Keep
            if let PokemonId = Pokemon.getPokemonId() {
                // Rename pokemon to its family name
                NewName = PokemonId.toString().capitalizedString
            }
        case 1: // Evolve
            // Rename pokemon according to user preferences
            NewName = SettingsViewController.EvoMark
        case 2: // Sell
            // Rename pokemon according to user preferences
            NewName = SettingsViewController.SellMark
        case 3: // Evolve + Sell
            // Rename pokemon according to user preferences
            NewName = SettingsViewController.EvoSellMark
        default:
            print("Segment selection not recognised")
        }
        self.Name.text = NewName
        
        // Ask the server to rename remotely
        if let ID = Pokemon.getID() {
            appDelegate.serverDelegate.PostNickSuccess = {
                // Modify Pokemon information and save to file
                if let PokemonId = self.Pokemon.getPokemonId() {
                    if let dataArray = self.appDelegate.serverDelegate.Pokemon[PokemonId.rawValue] {
                        for data in dataArray {
                            if let CompareID = data.getID() {
                                if ID == CompareID {
                                    data.setNickName(toValue: NewName)
                                    self.appDelegate.serverDelegate.saveState()
                                    break
                                }
                            }
                        }
                    }
                }
            }
            appDelegate.serverDelegate.askServer(request: .RenamePokemon, withPokemonId: ID, toNewName: NewName)
        }
    }
    
    // MARK: ViewController Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let CP = Pokemon.getCP() {
            self.CP.text = String(CP)
        }
        if let Move1 = Pokemon.getMove1() {
            self.MinorAttackName.text = Move1.toString().capitalizedString
            self.MinorAttackStrength.text = "--"
        }
        if let Move2 = Pokemon.getMove2() {
            self.MajorAttackName.text = Move2.toString().capitalizedString
            self.MajorAttackStrength.text = "--"
        }
        if let Weight = Pokemon.getWeight() {
            self.Weight.text = String(Weight) + " kg"
        }
        if let Height = Pokemon.getHeight() {
            self.Height.text = String(Height) + " m"
        }
        if let NickName = Pokemon.getNickName() {
            self.Name.text = NickName
            // Read TODOScheme from nickname
            switch NickName {
            case SettingsViewController.EvoMark:
                self.TODOScheme.selectedSegmentIndex = 1
            case SettingsViewController.SellMark:
                self.TODOScheme.selectedSegmentIndex = 2
            case SettingsViewController.EvoSellMark:
                self.TODOScheme.selectedSegmentIndex = 3
            default:
                self.TODOScheme.selectedSegmentIndex = 0
            }
        } else if let PokemonId = Pokemon.getPokemonId() {
            self.Name.text = PokemonId.toString().capitalizedString
        }
        if let PokemonId = Pokemon.getPokemonId() {
            self.Sprite.image = UIImage(named: PokemonId.toString().lowercaseString)
        }
        if let Attack = Pokemon.getIndividualAttack() {
            if let Defense = Pokemon.getIndividualDefense() {
                if let Stamina = Pokemon.getIndividualStamina() {
                    self.IV.updateIV(toAttack: Attack, toDefense: Defense, toStamina: Stamina)
                    let OverallScore = Int( CGFloat(Attack + Defense + Stamina)/(3.0*AppDelegate.MaxIV)*100.0 )
                    self.IVperc.text = String(OverallScore)+" %"
                    self.IV.setNeedsDisplay()
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
